import javax.swing.*;
import java.awt.*;

public class Window extends JFrame{

    private JLabel messageLabel;
    private JPanel grid;

    public Window(String firstSearchTooltipText){
        setBounds(100, 450, 500, 100);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setTitle("Первая подсказка при поиске в Яндексе");
        this.setLayout(new BorderLayout());
        // Создание панели с табличным расположением
        grid = new JPanel(new GridLayout(1, 1,1,1));
        this.add(grid, BorderLayout.CENTER);

        messageLabel = new JLabel(firstSearchTooltipText, JLabel.LEFT);
        this.add(messageLabel, BorderLayout.NORTH);

        this.setVisible(true);
    }

    public void close(){
        setVisible(false);
        dispose();
    }

}
