import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YandexMainPage {

    private WebDriver driver;

    public YandexMainPage(WebDriver driver){
        this.driver = driver;
    }

    public YandexMainPage setSearch(String query){
        WebElement searchInput = driver.findElement(By.id("text"));
        searchInput.sendKeys(query);
        return this;
    }

    public String getFirstSearchTooltip(){
        WebElement firstSearchTooltip = driver.findElement(By.xpath("(//span[@class='suggest2-item__text'])[1]"));
        String firstSearchTooltipText = firstSearchTooltip.getText();
        return firstSearchTooltipText;
    }

}
