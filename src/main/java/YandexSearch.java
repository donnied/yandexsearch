import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class YandexSearch {

    private WebDriver driver;
    private Window window;
    private String query;
    private String result;

    public YandexSearch(String query, String result) {
        this.query = query;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][]{
                {"погода", "погода спб"},
                {"Липецк", "липецк"},
                {"Лото", "лото"},
        });
    }

    @Test
    public void test() throws InterruptedException {
        driver.get("https://yandex.ru");
        YandexMainPage yandexMainPage = new YandexMainPage(driver);
        String firstSearchTooltipText = yandexMainPage.setSearch(query)
                .getFirstSearchTooltip();
        window = new Window(firstSearchTooltipText);
        System.out.println(firstSearchTooltipText);
        //assertEquals(result, firstSearchTooltipText);
        Thread.sleep(7000);
    }

    @Before
    public void setUp() {
        System.out.println("Start");
        String os = System.getProperty("os.name").toLowerCase();

    	if (os.indexOf("win") >= 0){
            System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    	}
    	else if (os.indexOf("nux") >= 0 || os.indexOf("nix") >= 0){
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver_linux");
    	}
    	else if (os.indexOf("mac") >= 0){
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
    	}
    	else{
    		System.out.println("Неизвестная ОС: " + os);
    	}

        driver = new ChromeDriver();
        driver.manage().window().setSize( new Dimension(800, 400));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
        window.close();
        System.out.println("End");
    }
}
